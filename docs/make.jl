using Documenter, BeerLabels

makedocs(
    modules = [BeerLabels],
    format = Documenter.HTML(; prettyurls = get(ENV, "CI", nothing) == "true"),
    authors = "Tamás Cserteg",
    sitename = "BeerLabels.jl",
    pages = Any["index.md"]
    # strict = true,
    # clean = true,
    # checkdocs = :exports,
)
