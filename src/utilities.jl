NORMAL_FONT_SIZE = 32
NAME_FONT_SIZE = 92
TYPE_FONT_SIZE = 54

"""
    struct LabelSize

A structure to store labelsize related infos.
"""
struct LabelSize
    dpmm::Int64
    xmm::Int64
    ymm::Int64
    xp::Int64
    yp::Int64
end

"""
    LabelSize(dpi, xmm, ymm)

`dpi` will be converted to dpmm, so it's just an approximate DPI.
"""
function LabelSize(dpi, xmm, ymm)
    dpmm = ceil(dpi * 0.03937008)
    return LabelSize(dpmm, xmm, ymm, dpmm*xmm, dpmm*ymm)
end

"""
    mmLP(x, y, ls)

`mmLP` is shorthand for mm label point.
Calculates the position of the given points (in mm) for the given label (`ls`).
"""
function mmLP(x, y, ls)
    (0 <= x && x <= ls.xmm) || throw(DomainError(x, "x should be 0<=x<=ls.xmm"))
    (0 <= y && y <= ls.ymm) || throw(DomainError(y, "y should be 0<=y<=ls.ymm"))
    return Point(x*ls.dpmm, y*ls.dpmm)
end

"""
    aLP(x, y, ls)

`aLP` is shorthand for absolute label point.
Calculates the position of the given points (fom 0 to 1) for the given label (`ls`).
"""
function aLP(x, y, ls)
    (0 <= x && x <= 1) || throw(DomainError(x, "x should be 0<=x<=1"))
    (0 <= y && y <= 1) || throw(DomainError(y, "y should be 0<=y<=1"))
    return Point(x*ls.xp, y*ls.yp)
end

"""Vitals position."""
vitalpos(ls::LabelSize) = aLP(0.82, 0.72, ls)

"""Name pos."""
namepos(ls::LabelSize) = aLP(0.5, 0.24, ls)

"""Tag position for minimal labels."""
mintag(ls::LabelSize) = aLP(0.09, 0.9, ls)

function vitals(ibu, balling, abv, ls)
    pos = vitalpos(ls)
    fontsize(NORMAL_FONT_SIZE)
    h_alignment = :center
    v_alignment = :middle
    text("IBU: $ibu", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("BALLING: $(balling)°", pos, halign=h_alignment , valign=v_alignment)
    text("ABV: $abv%", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
end

function nametag(ls::LabelSize, batchnum, bottleday)
    pos = vitalpos(ls) - aLP(0.53, 0, ls)
    fontsize(NORMAL_FONT_SIZE)
    h_alignment = :center
    v_alignment = :middle
    text("Főzet: No.$batchnum", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("Palackozva: $bottleday", pos, halign=h_alignment, valign=v_alignment)
    text("Sörfőző: Cserteg Tamás", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    #=
    text("Batch: No.$batchnum", tagpos+mmLP(0, 2.8, ls), halign=:left)
    text("@cserteGT3", halign=:center, valign = :bottom)
    @layer begin
        scale(0.1)
        julialogo()
    end
    =#
    origin()
end

"""
    bottlenametag(ls::LabelSize, batchnum, bottleday)

Nametag with place for bottle numbering.
"""
function bottlenametag(ls::LabelSize, batchnum, bottleday)
    pos = vitalpos(ls) - aLP(0.53, 0, ls)
    fontsize(NORMAL_FONT_SIZE)
    h_alignment = :center
    v_alignment = :middle
    text("Palack: No.$batchnum, __/__", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("Palackozva: $bottleday", pos, halign=h_alignment, valign=v_alignment)
    text("Sörfőző: Cserteg Tamás", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    origin()
end

function minimal_nametag(ls::LabelSize, batchnum, bottleday)
    pos = mintag(ls)
    fontsize(NORMAL_FONT_SIZE)
    h_alignment = :left
    v_alignment = :left
    #text("Batch: #$batchnum", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    #text("Bottled: $bottleday", pos, halign=h_alignment, valign=v_alignment)
    #text("Sörfőző: Cserteg Tamás", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("Sörfőző: Cserteg Tamás", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    #=
    text("Batch: No.$batchnum", tagpos+mmLP(0, 2.8, ls), halign=:left)
    text("@cserteGT3", halign=:center, valign = :bottom)
    @layer begin
        scale(0.1)
        julialogo()
    end
    =#
    origin()
end

function beername(name, style, ls)
    h_alignment = :center
    fontsize(NAME_FONT_SIZE)
    text(name, namepos(ls), halign=h_alignment)
    fontsize(TYPE_FONT_SIZE)
    text(style, namepos(ls)+aLP(0, 0.114, ls), halign=h_alignment)
end

function minimal_beername(style, abvval, ls)
    h_alignment = :center
    fontsize(NAME_FONT_SIZE)
    text(style, namepos(ls)+aLP(0, 0.1, ls), halign=h_alignment)
    fontsize(TYPE_FONT_SIZE)
    text(abvval*"%", namepos(ls)+aLP(0, 0.214, ls), halign=h_alignment)
end


"""
    printlabel(labelfunc, filename)

# Example

```julia-repl
julia> printlabel(__label_no11, "testpage.pdf")

julia> printlabel(__label_no11, "testpage.png")
````
"""
function printlabel(labelfunc, filename)
    # A4 - 300 dpi
    xsize = 2480
    ysize = 3508
    Drawing(xsize, ysize, filename)
    
    ls = LabelSize(300, 92, 70)

    setcolor("black")
    setline(1)

    ## 1st row - top
    hdif11 = 100
    vdif11 = 450
    
    @layer begin
        origin(hdif11, vdif11)
        labelfunc()
    end

    hdif12 = hdif11 + 100
    vdif12 = vdif11
    
    @layer begin
        origin(hdif12+ls.xp, vdif12)
        labelfunc()
    end
    
    ## 2nd row - middle
    
    hdif21 = 100
    vdif21 = vdif11+ls.yp+100
    
    @layer begin
        origin(hdif21, vdif21)
        labelfunc()
    end
    
    hdif22 = hdif21 + 100
    vdif22 = vdif21
    
    @layer begin
        origin(hdif22+ls.xp, vdif22)
        labelfunc()
    end
    
    ## 3rd row - bottom
    
    hdif31 = 100
    vdif31 = vdif21+ls.yp+100
    
    @layer begin
        origin(hdif31, vdif31)
        labelfunc()
    end
    
    hdif32 = hdif31 + 100
    vdif32 = vdif31
    
    @layer begin
        origin(hdif32+ls.xp, vdif32)
        labelfunc()
    end

    # draw lines after the labels

    # horizontal above of first row
    line(Point(0, vdif11), Point(xsize, vdif11), :stroke) # use this
    # horizontal below first row
    line(Point(0, vdif11+ls.yp), Point(xsize, vdif11+ls.yp), :stroke)
    
    # vertical left to first column
    line(Point(hdif11, 0), Point(hdif11, ysize), :stroke) # use this
    # vertical right next to first column
    line(Point(hdif11+ls.xp, 0), Point(hdif11+ls.xp, ysize), :stroke)
    # vertical left to second column
    line(Point(hdif12+ls.xp, 0), Point(hdif12+ls.xp, ysize), :stroke)
    # vertical right to second column
    line(Point(hdif12+2*ls.xp, 0), Point(hdif12+2*ls.xp, ysize), :stroke)
    # horizontal above of second row
    line(Point(0, vdif21), Point(xsize, vdif21), :stroke)
    # horizontal below of second row
    line(Point(0, vdif21+ls.yp), Point(xsize, vdif21+ls.yp), :stroke)
    # horizontal above of third row
    line(Point(0, vdif31), Point(xsize, vdif31), :stroke)
    # horizontal below of third row
    line(Point(0, vdif31+ls.yp), Point(xsize, vdif31+ls.yp), :stroke)
    
    finish()
end
