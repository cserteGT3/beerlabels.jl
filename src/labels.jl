## label 10

function label_no10(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no10()
    finish()
end

"""
    __label_no10()

Call the functions to crate the label for beer #10.
"""
function __label_no10()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    
    ## text
    fontface("JuliaMono")
    setcolor("black")
    beername("Legjobb Rózsika", "Best Bitter", ls)
    vitals(30, 11, 4.5, ls)
    nametag(ls, 10, "2021.03.10.")
end

## label 11

function label_no11(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no11_white()
    finish()
end

"""
    __label_no11()

Call the functions to crate the label for beer #11.
"""
function __label_no11()
    ls = LabelSize(300, 92, 70)
    ## background
    setmode("source")
    gray2gray = blend(Point(ls.xp/2, 0), Point(ls.xp/2, ls.yp), "grey12", "grey2")
    setblend(gray2gray)
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    #background("gray")

    ## text
    fontface("JuliaMono")
    setcolor("grey65")
    beername("RIS 60", "Imperial Stout", ls)
    vitals(107, 22, 9.8, ls)
    nametag(ls, 11, "2021.03.26.")
end

"""
    __label_no11_white()

Call the functions to crate the label for beer #11 with white background.
"""
function __label_no11_white()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    ## text
    fontface("JuliaMono")
    setcolor("black")
    beername("RIS 60", "Imperial Stout", ls)
    vitals(107, 22, 9.8, ls)
    nametag(ls, 11, "2021.03.26.")
end

function label_no11_alt(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no11_alt()
    finish()
end

"""
    __label_no11()

Call the functions to crate the label for beer #11.
"""
function __label_no11_alt()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    ## text
    fontface("Times New Roman")
    setcolor("black")
    minimal_beername("Imperial Stout", "9.8" ,ls)
    #vitals(107, 22, 9.8, ls)
    minimal_nametag(ls, 11, "2021.03.26.")
end

## label 16

function label_no16(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no16()
    finish()
end

"""
    __label_no16()

Call the functions to crate the label for beer #16.
"""
function __label_no16()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    
    ## text
    fontface("Times New Roman")
    setcolor("black")
    beername("SzaPh(a)Dság", "Barleywine", ls)
    #beername("Szaph(a)dság", "Barleywine", ls)
    vitals(70, 26.2, 12.5, ls)
    bottlenametag(ls, 16, "2021.12.04.")
end

## label 20

function label_no20(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no20()
    finish()
end

"""
    __label_no20()

Call the functions to crate the label for beer #20.
"""
function __label_no20()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)

    ## text
    fontface("Times New Roman")
    setcolor("black")
    beername("Voss", "Imperial Stout", ls)
    vitals(73, 23.5, 10.2, ls)
    #beername("Stranda", "Imperial Stout", ls)
    #vitals(73, 23.5, 10.5, ls)
    nametag(ls, 20, "2022.01.06.")
end

## label 26

function label_no26(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no26()
    finish()
end

"""
    __label_no26()

Call the functions to crate the label for beer #26.
"""
function __label_no26()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)

    ## text
    fontface("Times New Roman")
    setcolor("black")
    beername("RIS 602", "Imperial Stout", ls)
    vitals(70, 23.1, 10.2, ls)
    #beername("Stranda", "Imperial Stout", ls)
    #vitals(73, 23.5, 10.5, ls)
    nametag(ls, 26, "2022.05.11.")
end

## label 39

function label_no39(filename)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, filename)
    __label_no39()
    finish()
end

"""
    __label_no39()

Call the functions to crate the label for beer #39.
"""
function __label_no39()
    ls = LabelSize(300, 92, 70)
    ## background
    sethue("white")
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)

    ## text
    fontface("Times New Roman")
    setcolor("black")
    beername("Trip el", "Belga Tripel", ls)
    vitals(31, 20.7, 10, ls)
    bottlenametag(ls, 39, "2024.07.20.")
end
