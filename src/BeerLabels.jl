module BeerLabels

using Dates: DateFormat, DateTime

using Luxor
using Colors

export  __label_no10,
        __label_no11,
        __label_no11_alt,
        __label_no11_white,
        __label_no16,
        __label_no20,
        __label_no26,
        __label_no39,
        label_no10,
        label_no11,
        label_no11_alt,
        label_no16,
        label_no20,
        label_no26,
        label_no39,
        printlabel

include("utilities.jl")
include("labels.jl")

end # module
