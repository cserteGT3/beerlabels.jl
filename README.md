# BeerLabels.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://csertegt3.gitlab.io/BeerLabels.jl/)
[![Build Status](https://gitlab.com/cserteGT3/BeerLabels.jl/badges/master/pipeline.svg)](https://gitlab.com/cserteGT3/BeerLabels.jl/pipelines)
[![Coverage](https://img.shields.io/codecov/c/gitlab/cserteGT3/BeerLabels.jl)](https://app.codecov.io/gl/cserteGT3/BeerLabels.jl)

## Changelog

### v1.2.0

Used for: 26.

### v1.1.0

Used for: 20.

Changes:

* Főzet instead of batch.

### v1.0.0

Some finalization to tag the version used for batch no.16.

## Notes

My printer has a setting for margins 25 mm each side, which is around 1 inch.
